//app.js
App({
  onLaunch: function () {
    // 展示本地存储能力
    let time = new Date()
    this.agData = wx.getStorageSync('agData') ||
     {
      hp: 100,
      food: 0,
      happy: 0,
      gifts: 0,
      money: 0,
      foodGift: 1,
      giftGift: 2,
      loveGift: 2,
      hurtGift: 1,
      today: time.getFullYear() + time.getMonth() + time.getDate(),
      items: {}
    }
  }
})