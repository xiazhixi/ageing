/**
 * 游戏脚本
 * type: ''
 */
module.exports = {
    GameScript: {
        hi1: {
            type: 'notice',
            content: '阿ging: 欢迎来到阿ging的宝库！让我猜猜看，你就是传说的镕儿吧！',
            text: ['对的'],
            action: ['hi2']
        },
        hi2: {
            type: 'choose',
            content: '阿ging: 在这里你能获得很多的宝物哦！不过你需要陪我玩游戏才可以~',
            text: ['呵呵。', '好啊~'],
            action: ['hi3', 'hi4']
        },
        hi3: {
            type: 'notice',
            content: '阿ging: 你好像不太开心……',
            text: ['想多了.'],
            action: ['hi5']
        },
        hi4: {
            type: 'notice',
            content: '阿ging: 那么~~~~~',
            text: ['那么？'],
            action: ['hi5']
        },
        hi5: {
            type: 'notice',
            content: '阿ging: 那我们开始吧~！相信聪明的你随便戳戳就知道该怎么做咯！',
            text: ['好的'],
            action: ['']
        },
        dead1: {
            type: 'notice',
            content: '阿ging已经死了, 再也见不到阿ging了',
            text: ['这样啊.'],
            action: ['']
        },
        dead2: {
            type: 'notice',
            content: '阿ging活得太痛苦了，自杀了',
            text: ['这样啊.'],
            action: ['']
        },
        hurt1: {
            type: 'choice',
            content: '阿ging一动不动没有反应',
            text: ['治疗他', '打他'],
            action: ['help', 'beat']
        },
        hurt2: {
            type: 'notice',
            content: '阿ging受伤过重死掉了',
            text: ['哦'],
            action: ['']
        },
        hurt3: {
            type: 'notice',
            content: '阿ging慢慢的醒了过来',
            text: ['知道了'],
            action: ['']
        },
        beg1: {
            type: 'choose',
            content: '阿ging: 求求你不要伤害我，我给你宝物',
            text: ['好吧', '呵呵'],
            action: ['beatAward', 'beat']
        },
        beg2: {
            type: 'choose',
            content: '阿ging: 我没有宝物了!不要打我 QAQ, 怕疼',
            text: ['放过他', '残忍动手'],
            action: ['', 'beat']
        },
        thanks1: {
            type: 'choose',
            content: '阿ging: 哇，好开心，这是给我的吗？',
            text: ['你想多了', '是的呀'],
            action: ['hurt', 'giftAward']
        },
        thanks2: {
            type: 'choose',
            content: '阿ging: 你要给我礼物吗？我没礼物咯~',
            text: ['你想多了', '没关系'],
            action: ['hurt', 'encourage']
        },
        thanks3: {
            type: 'choose',
            content: '阿ging: 摸摸很舒服~ 我喜欢摸摸~',
            text: ['你想多了', '揉搓揉搓'],
            action: ['hurt', 'loveAward']
        },
        thanks4: {
            type: 'choose',
            content: '阿ging: 摸摸很舒服~ 但是我没东西给你咯~',
            text: ['你想多了', '揉搓揉搓'],
            action: ['hurt', 'encourage']
        },
        feed1: {
            type: 'choose',
            content: '阿ging: 哇，好吃的！给我的吗？',
            text: ['你想多了', '给你~'],
            action: ['hurt', 'feedAward']
        },
        feed2: {
            type: 'choose',
            content: '阿ging: 好吃的！不过我没礼物了~',
            text: ['你想多了', '给你~'],
            action: ['hurt', 'feed']
        }
    },

    Gifts: [
        {
            name: '香奈儿挎包',
            icon: '//img.alicdn.com/imgextra/TB1JvtyaUD.BuNjt_h7L6SNDVXa_430x430q90.jpg',
            price: 35570,
            link: 'https://jd.com',
            rare: '4',
            index: 0
        },
        {
            name: 'ipad pro 2018',
            icon: 'https://img.alicdn.com/imgextra/i1/1669409267/TB2UnhgpN1YBuNjy1zcXXbNcXXa_!!1669409267.jpg_430x430q90.jpg',
            price: 6000,
            link: 'https://jd.com',
            rare: '4',
            index: 1
        },
        {
            name: '乐高哈利波特大城堡',
            icon: 'https://gd2.alicdn.com/imgextra/i2/90701524/O1CN011N823S9iJyZ3SXH_!!90701524.jpg',
            price: 3499,
            link: 'https://jd.com',
            rare: '3',
            index: 2
        },
        {
            name: '海蓝之谜 凝霜60ml',
            icon: 'https://img14.360buyimg.com/n0/2154/5f678995-9a86-4281-82c5-364ba154cf33.jpg',
            price: 2550,
            link: 'https://jd.com',
            rare: '3',
            index: 3
        },
        {
            name: 'refa 全身升级版',
            icon: 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1535123006583&di=de9764b6205faee4d27ea18115a41bc9&imgtype=0&src=http%3A%2F%2Fimage1.suning.cn%2Fuimg%2Fb2c%2Fnewcatentries%2F0070168634-000000000671403727_5_800x800.jpg',
            price: 1690,
            link: 'https://jd.com',
            rare: '2',
            index: 4
        },
        {
            name: '海蓝之谜 凝霜30ml',
            icon: 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1535123038867&di=ece83a263a00f1db1e09d039c584552b&imgtype=0&src=http%3A%2F%2Fb.hiphotos.baidu.com%2Fzhidao%2Fpic%2Fitem%2F0b46f21fbe096b634f97e9c407338744eaf8ac9c.jpg',
            price: 908,
            link: 'https://jd.com',
            rare: '2',
            index: 5
        },
        {
            name: '蛇胆花露水 * 40',
            icon: 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1535123068096&di=33f25b3e78501ce542ae33d00b049042&imgtype=0&src=http%3A%2F%2Fwww.edazhang.com%2Fuploadb%2Fimage%2F201406%2Fbig%2F8f2c77c7581243429d519352b9eac19b.jpg',
            price: 130,
            link: 'https://jd.com',
            rare: '1',
            index: 6
        },
        {
            name: '100元代金券',
            icon: 'https://gd1.alicdn.com/imgextra/i1/137180252/TB23av2fx3IL1JjSZPfXXcrUVXa_!!137180252.png_400x400.jpg',
            price: 100,
            link: 'https://jd.com',
            rare: '1',
            index: 7
        }
    ]
}