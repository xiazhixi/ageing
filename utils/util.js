import {GameScript} from './script'
const app = getApp()
module.exports = {
  saveGame: () => {
    wx.setStorageSync('agData', app.agData)
  },
  newDay: res => {
    let time = new Date()
    let today = time.getFullYear() + time.getMonth() + time.getDate()
    if (res.today !== today && res.hp > 0) {
      res.today = today
      res.hp = res.hp + 20 > 100? 100: res.hp + 20
      res.food = res.food - 30 < 0 ? 0: res.food - 30
      res.foodGift = 1
      res.giftGift = 2
      res.loveGift = 2
      res.hurtGift = 1
      app.agData = res
      this.saveGame()
      return res
    } else {
      return false
    }
  },
  callScript: name => {
    let controller = GameScript[name]
    return controller
  }
}
