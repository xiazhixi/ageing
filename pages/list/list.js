import {
    Gifts
} from '../../utils/script'
import {
    saveGame
} from '../../utils/util'
const app = getApp()
let rareList = {
    1: [],
    2: [],
    3: [],
    4: []
}
Page({
    data: {
        agData: app.agData,
        Gifts
    },
    onLoad: function () {
        for (let i = 0; i < Gifts.length; i++) {
            let data = Gifts[i]
            rareList[data.rare].push(Gifts[i].index)
        }
    },
    roll: function (times) {
        let query = []
        while (times--) {
            let num = Math.random() * 100
            let rare = 1
            if (num > 65) rare = 2
            if (num > 95) rare = 3
            if (num > 99.5) rare = 4
            while (rareList[rare].length === 0) {
                rare--
            }
            query.push(rare)
        }
        let result = []
        query.map(res => {
            let num = rareList[res].length
            num = parseInt(Math.random() * (num - 0.01))
            let index = rareList[res][num]
            if (app.agData.items[index]) {
                app.agData.items[index]++
            } else {
                app.agData.items[index] = 1
            }
            result.push(index)
            return index
        })
        return result 
    },
    showMenu: function (e) {
        let data = e.target.dataset
        let index = data.index
        let num = app.agData.items[index]
        if (num) {
            wx.showActionSheet({
                itemList: ['出售'],
                success: res => {
                    if (res.tapIndex === 0) {
                        wx.showModal({
                            title: '请确认',
                            content: `是否以${data.item.price}的价格出售${data.item.name}? \n 注意出售后无法撤销!`,
                            success: res => {
                                if (res.confirm) {
                                    app.agData.items[index]--
                                        app.agData.money = parseInt(app.agData.money) + parseInt(data.item.price)
                                    this.updateInfo()
                                }
                            }
                        })
                    }
                }
            })
        } else {
            wx.showActionSheet({
                itemList: ['购买'],
                success: res => {
                    if (res.tapIndex === 0) {
                        if (app.agData.money >= data.item.price) {
                            wx.showModal({
                                title: '请确认',
                                content: `是否以${data.item.price}的价格购买${data.item.name}?`,
                                success: res => {
                                    if (res.confirm) {
                                        if (app.agData.items[index]) {
                                            app.agData.items[index]++
                                        } else {
                                            app.agData.items[index] = 1
                                        }
                                        app.agData.money = parseInt(app.agData.money) - parseInt(data.item.price)
                                        this.updateInfo()
                                    }
                                }
                            })
                        } else {
                            wx.showModal({
                                title: '提示',
                                content: '您的余额不足',
                                showCancel: false
                            })
                        }
                    }
                }
            })
        }
    },
    openGift: function () {
        if (app.agData.gifts === 0) return false
        app.agData.gifts--
        let result = this.roll(3)
        let content = '恭喜你获得了\n'
        result.map(item => {
            let data = Gifts.filter(res => {
                return res.index === item
            })[0]
            content += `${data.name} x 1 \n`
        })
        wx.showModal({
            title: '提示',
            content,
            confirmText: app.agData.gifts ? '继续抽奖' : '我知道了',
            cancelText: '就这样吧',
            showCancel: app.agData.gifts,
            success: res => {
                if (res.confirm) {
                    this.openGift()
                }
            }
        })
        this.updateInfo()
    },
    updateInfo: function () {
        this.setData({
            agData: app.agData
        })
        saveGame()
    }
})