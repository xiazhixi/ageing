//index.js
//获取应用实例
const app = getApp()
let that
import {
  newDay,
  saveGame,
  callScript
} from '../../utils/util'

Page({
  data: {
    agData: app.agData
  },
  openBag: function () {
    wx.navigateTo({
      url: '/pages/list/list'
    })
  },
  testScript: function (name) {
    if (this.actions[name]) {
      this.actions[name]()
    } else {
      let script = callScript(name)
      script && this.handleScript(script)
    }
  },
  handleScript: function (script) {
    wx.showModal({
      title: '提示',
      content: script.content,
      showCancel: script.type !== 'notice',
      cancelText: script.text[1] || '',
      confirmText: script.text[0],
      success: res => {
        if (res.confirm) {
          this.testScript(script.action[0])
        } else {
          this.testScript(script.action[1])
        }
      }
    })
  },
  getGift: function () {
    wx.showToast({
      icon: 'success',
      title: '你获得了礼盒x1'
    })
    app.agData.gifts++
  },
  testState: function () {
    if (app.agData.hp <= 0) {
      this.handleScript(callScript('hurt2'))
    }
    if (app.agData.hp > 100) {
      app.agData.hp = 100
    }
    if (app.agData.happy > 100) {
      app.agData.happy = 100
    }
    if (app.agData.food > 200) {
      app.agData.food = 200
    }
    if (app.agData.happy < -100) {
      this.handleScript(callScript('dead2'))
      app.agData.hp = 0
    }
  },
  actions: {
    feedAward: function () {
      --app.agData.foodGift
      app.agData.happy += 15
      app.agData.food += 15
      that.getGift()
      that.updateData()
    },
    beatAward: function () {
      --app.agData.hurtGift
      that.getGift()
      that.updateData()
    },
    beat: function () {
      app.agData.hp -= 16
      app.agData.happy -= 14
      wx.showToast({
        icon: 'none',
        title: '阿ging糟到了毒打'
      })
      that.updateData()
    },
    feed: function () {
      app.agData.food += 15
      wx.showToast({
        icon: 'none',
        title: '阿ging变开心(pang)了'
      })
      that.updateData()
    },
    help: function () {
      app.agData.hp += 25
      wx.showToast({
        icon: 'none',
        title: '阿ging体力恢复了'
      })
      that.updateData()
    },
    loveAward: function () {
      --app.agData.loveGift
      ++app.gifts
      app.agData.happy += 15
      that.getGift()
      that.updateData()
    },
    giftAward: function () {
      --app.agData.giftGift
      ++app.gifts
      app.agData.happy += 15
      that.getGift()
      that.updateData()
    },
    encourage: function () {
      app.agData.happy += 10
      that.testState()
      wx.showToast({
        icon: 'none',
        title: '阿ging变得开心了'
      })
      that.updateData()
    },
    hurt: function () {
      app.agData.happy -= 10
      wx.showToast({
        icon: 'none',
        title: '阿ging变得难过了'
      })
      that.updateData()
    }
  },
  updateData: function () {
    this.testState()
    let newData = newDay(this.data.agData)
    if (!newData) saveGame()
    this.setData({
      agData: app.agData
    })
  },
  onShow: function () {
    this.updateData()
  },
  onLoad: function () {
    that = this
    if (!wx.getStorageSync('firstplay')) {
      wx.setStorageSync('firstplay', true)
      this.handleScript(callScript('hi1'))
    }
  },
  action: function (e) {
    if (app.agData.hp <= 0) {
      this.handleScript(callScript('dead1'))
      return false
    }
    if (app.agData.hp < 30) {
      this.handleScript(callScript('hurt1'))
      return false
    }
    let index = e.target.dataset.index
    let name
    switch (index) {
      case '0':
        name = app.agData.foodGift > 0 ? 'feed1' : 'feed2'
        break
      case '1':
        name = app.agData.loveGift > 0 ? 'thanks3' : 'thanks4'
        break
      case '2':
        name = app.agData.giftGift > 0 ? 'thanks1' : 'thanks2'
        break
      case '3':
        name = app.agData.hurtGift > 0 ? 'beg1' : 'beg2'
        break
    }
    let script = callScript(name)
    this.handleScript(script)
  }
})